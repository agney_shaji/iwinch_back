# -*- coding: utf-8 -*-
# Copyright (c) 2019, Agney and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from notifications.fcm import fcm_send_message

class AppNotifications(Document):


	def before_save(self):
		print('before_save before_save')

	def before_insert(self):	
		print("This is called before a document is inserted into the database.")


	def after_insert(self):
		print('innnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn')
		registration_id = frappe.db.get_value('Devices', {'user': self.recipient}, ['token'])
		print(self.recipient)	
		result = fcm_send_message(
            registration_id=registration_id,
            title=self.title,
            body=self.message,
            data={'order_table':self.order_table, 'order_name':self.order_name, 'noti_type':self.noti_type}
        )
		print(result)
