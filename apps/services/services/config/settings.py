from __future__ import unicode_literals
from frappe import _

# def get_data():
# 	data = [
# # 		# {
# # 		# 	"label": _("Services"),
# # 		# 	"icon": "fa fa-wrench",
# # 		# 	# "items": [
# # 		# 	# 	{
# # 		# 	# 		"type": "doctype",
# # 		# 	# 		"name": "Service Settings",
# # 		# 	# 		"label": _("Service Settings"),
# # 		# 	# 		"description": _("Language, Date and Time settings"),
# # 		# 	# 	},
# #         #     # ],
# #         # },
#     ]

    
def get_data():
	return [
		{
			"label": _("Services"),
			"icon": "fa fa-star",
			"items": [
				{
					"type": "doctype",
					"name": "Service Regions Settings",
					"label": _("Service Settings"),
					"description": _("Set default values for Base Price  and Commission Percentag for each service"),
				},
			]
		},
	]