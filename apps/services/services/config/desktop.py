# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Services",
			"color": "yellow",
			"icon": "fa fa-puzzle-piece",
			"type": "module",
			"reverse": 1,
			"label": _("Services"),
		}
	]
