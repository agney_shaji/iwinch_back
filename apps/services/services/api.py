from __future__ import unicode_literals
import frappe
# from frappe.utils.nestedset import NestedSet, get_ancestors_of, get_descendants_of


@frappe.whitelist()
def get_services(doctype='', parent='', **filters):
    # print(frappe.form_dict)
    doctype = 'Services'
    parent_field = 'parent_' + doctype.lower().replace(' ', '_')
    filters = [['ifnull(`{0}`,"")'.format(parent_field), '=', parent]]

    doctype_meta = frappe.get_meta(doctype)
    data = frappe.get_list(doctype, fields=[
        'name',
        'service_name as title',
        'image',
        'service_type',
        'service_desc',
        'is_group as expandable'],
        filters=filters,
        order_by='name')

    return data


@frappe.whitelist()
def get_service_detail(service_name):
    # print(frappe.form_dict)
    doctype = 'Services'
    print(service_name, '----------------------------------------------')
    data = frappe.db.get_value(doctype, service_name, ['name', 'service_name', 'service_type'], as_dict=1)
    return data


@frappe.whitelist()
def search_services(service_name='', limit_start=0):
    # print(frappe.form_dict)
    doctype = 'Services'
    filters = [['docstatus', '<', '2'], ['service_name', 'like', '{}%'.format(service_name)],
               ['is_group', '=', '0']]

    data = frappe.get_list(doctype, fields=[
        'name',
        'service_name as title',
        'image',
        'service_desc',
        'service_type',
        'is_group as expandable'],
        filters=filters,
        order_by='name',
        limit_start=limit_start)

    return data