from __future__ import unicode_literals
import frappe
from frappe.utils.nestedset import get_descendants_of


@frappe.whitelist()
def get_offers(offer_type="Offer"):
    # print(frappe.form_dict)
    doctype = 'Offers'
    filters = [['type', '=', '{}'.format(offer_type)]]
    # doctype_meta = frappe.get_meta(doctype)
    data = frappe.get_list(doctype, fields=[
        'name',
        'type',
        'title',
        're_link',
        'disc_img',
        'coupon_code'],
        filters=filters,
        order_by='name')
    return data

@frappe.whitelist()
def get_all_offers(service_name='All'):
    # print(frappe.form_dict)
    doctype = 'Offers'
    filters = [['docstatus', '<', '2']]
    if service_name != 'All':
        filters.append(['temp_service', '=', service_name])
    doctype_meta = frappe.get_meta(doctype)
    data = frappe.get_list(doctype, fields=[
        'name',
        'type',
        'title',
        're_link',
        'disc_img',
        'coupon_code'],
        filters=filters,
        order_by='name')
    return data


@frappe.whitelist()
def offer_detail_coupon(coupon_code):
    offer_name = frappe.db.sql("select name from `tabOffers` where coupon_code=%s",(coupon_code,))
    try:
        offer = frappe.get_doc('Offers', offer_name[0][0])
        return offer
    except expression as identifier:
        return None

@frappe.whitelist()
def offer_detail(offer_name):
    offer = frappe.get_doc('Offers', offer_name)
    return offer
    


@frappe.whitelist()
def get_sub_offers(parent=""):
    # print(frappe.form_dict)
    sub_services = get_descendants_of('Services', parent)
    doctype = 'Offers'
    filters = [['temp_service', 'in', ','.join(sub_services)]]
    # doctype_meta = frappe.get_meta(doctype)
    data = frappe.get_list(doctype, fields=[
        'name',
        'type',
        'title',
        're_link',
        'disc_img',
        'coupon_code'],
        filters=filters,
        order_by='name')
    return data