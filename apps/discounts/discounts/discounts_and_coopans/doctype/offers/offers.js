// Copyright (c) 2019, Agney Shaji and contributors
// For license information, please see license.txt

frappe.ui.form.on('Offers', {
	setup: function(frm) {
		frm.set_query("offer_service", function() {
			return {
				filters: {"is_group": 0}
			}
		});
	},
});
