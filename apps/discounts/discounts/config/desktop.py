# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Discounts And Coopans",
			"color": "grey",
			"icon": "fa fa-gift",
			"type": "module",
			"label": _("Offers"),
		}
	]
