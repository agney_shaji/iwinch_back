from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{	
			"label": _("Offers"),
			"icon": "fa fa-star",
			"items": [
				{
					"type": "doctype",
					"name": "Offers",
					"label": _("Offers List"),
					"description": _("Offers List"),
				}
			],
		},
	]
