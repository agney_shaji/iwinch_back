from __future__ import unicode_literals
import frappe
import json
from frappe.utils import nowdate, today, date_diff, now
from profiles.api import get_logged_provider


@frappe.whitelist()
def request_payment(**kargs):
    driver = get_logged_provider()
    now_date = now()
    if date_diff(now_date, driver.creation) < 15:
        if not frappe.db.exists({ 'doctype': 'Payment Request', 'reqs_by': frappe.session.user, 'docstatus': 0}):

            if frappe.db.exists('Payment Profile', frappe.session.user):
                pay_profile = frappe.get_doc('Payment Profile', frappe.session.user)
                from_date = pay_profile.l_ap_date
            else:
                print('new profile')
                pay_profile = frappe.get_doc({
                            'doctype': 'Payment Profile',
                            'account_manager': frappe.session.user,
                        })
                pay_profile.flags.ignore_permissions = True
                pay_profile.save()
                from_date = driver.creation
            
            amount = frappe.db.sql("select SUM(`grand_total`) as total_eranings from `tabService Invoice` where provider=%s and status='Closed' and creation >= %s",
                        (frappe.session.user, from_date), as_dict=True)

            request = frappe.get_doc({
                'doctype': 'Payment Request',
                'reqs_by': frappe.session.user,
                'reqs_on': now_date,
                'amt': amount[0]['total_eranings'],
            })

            request.flags.ignore_permissions = True
            request.save()

            pay_profile.app_amt = pay_profile.app_amt + request.amt
            pay_profile.l_re_date = now_date
            pay_profile.save()
        
            return {"status": True, "pay_profile": pay_profile, "request": request}

        return{"status": False, "error": "Last Request Pending For Approval"}

    return{"status": False, "error": "Please Request after 15 Days Of Joining"}