# -*- coding: utf-8 -*-
# Copyright (c) 2020, agney and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.utils import now
from frappe import _

class PaymentRequest(Document):

	def before_submit(self):
		if self.is_approved == 1:
			aapr_on = now()
			self.appr_by = frappe.session.user
			self.aapr_on = aapr_on
			pay_profile = frappe.get_doc('Payment Profile', self.reqs_by)
			pay_profile.l_ap_date = self.reqs_on
			pay_profile.app_amt = pay_profile.app_amt + self.amt
			pay_profile.save()
		else:
			frappe.throw(_("Please approve to submit else delete the document for rejecting it"))

	def on_cancel(self):	
		frappe.throw(_("Cannot cancel submitted Request"))


				


			