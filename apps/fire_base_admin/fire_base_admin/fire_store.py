import firebase_admin
from firebase_admin import credentials, firestore, initialize_app
import os

if (not len(firebase_admin._apps)):
    __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    cred = credentials.Certificate(os.path.join(__location__, 'iwinch-firebase-adminsdk.json'))
    default_app = initialize_app(cred)
    
db = firestore.client()
pro_ref = db.collection('providers')
order_ref = db.collection('orders')


def create_provider(provider):
    # db = firebase.database()
    
    data = {
            "provider_name": str(provider.full_name),
            "image": provider.image,
            "status": "open",
            "mobile_no": provider.mobile_no,
            "vh_type": provider.vh_type
            }

    print(data)
    result = pro_ref.document(provider.name).set(data)
    print('not setttttttttttttttttttttttttttttt', result)


def delete_provider(provider):
    result = pro_ref.document(provider.name).delete()
    print('not setttttttttttttttttttttttttttttt', result)

def update_status(provider, status):
    result = pro_ref.document(provider).update({'status': status})



def create_order(order):
    
    data = {
            "customer": order.customer,
            "status": order.status,
            "start_lat": float(order.start_lat),
            "start_lon": float(order.start_lon),
            "end_lat": float(order.end_lat),
            "end_lon": float(order.end_lon),
            }
    result = order_ref.document(order.name).set(data)


def delete_order(order):
    result = order_ref.document(order.name).delete()


def get_real_providers(order, vehecle, first=False):

    providers = pro_ref.where( u'status', u'==', u'open').where(u'vh_type', u'==', u'{}'.format(vehecle)).get()

    providers_list = list()
    
    for doc in providers:
        pro_dict = doc.to_dict()
        pro_dict["id"] = doc.id
        providers_list+=[pro_dict]
    if first and len(providers_list) > 0:
        update_status(providers_list[0]['id'], "engage")

    return providers_list