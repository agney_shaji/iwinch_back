import pyrebase
import os
# from frappe.utils.background_jobs import enqueue


__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
config = {
    "apiKey": "apiKey",
    "authDomain": "iwinch.firebaseapp.com",
    "databaseURL": "https://iwinch.firebaseio.com",
    "storageBucket": "iwinch.appspot.com",
    "serviceAccount": os.path.join(__location__, 'iwinch-firebase-adminsdk.json')
    }
firebase = pyrebase.initialize_app(config)


def change_order_status(order, status):
    db = firebase.database()
    result = db.child("orders").child(order).update({"status": status})
    return result


def create_order_invoice(order, invoice_no, status):
    db = firebase.database()
    result = db.child("orders").child(order).update({"invoice_no": invoice_no, "status": status})
    return result

def delete_order(order):
    db = firebase.database()
    result = db.child("orders").child(order).remove()
    return result


def change_provider_status(order, status):
    db = firebase.database()
    result = db.child("orders").child(order).update({"status": status})
    return result

def update_order(order):
    db = firebase.database()
    result = db.child("orders").child(order.name).update({"status": order.status, "provider": order.provider})
    return result

def create_order(order):
    db = firebase.database()
    data = {
            "customer": order.customer,
            "status": order.status,
            "customer_phone": order.cus_phno,
            "total_qty": order.total_qty,
            "total": order.grand_total,
            "start_lat": float(order.start_lat),
            "start_lon": float(order.start_lon),
            "end_lat": float(order.end_lat),
            "end_lon": float(order.end_lon),
            "ride_date": order.ride_date,
            "ride_type": order.ride_type
            }
    result = db.child("orders").child(order.name).set(data)
    return result


def create_notification(notification):
    db = firebase.database()
    data = {
            'title': notification.title,
            'sender': notification.sender,
            'recipient': notification.recipient,
            'message': notification.message,
            'order_table': notification.order_table,
            'order_name': notification.order_name,
        }
    result = db.child("notifications").push(data)
    return result


def update_provider(order, provider):
    db = firebase.database()
    result = db.child("orders").child(order).update({"provider": provider})
    return result


class Allocaton:
    """pyrebase Bug canbe resolved by inheriting  stream"""

    def __init__(self, order):
        db = firebase.database()
        # self.build_headers = build_headers
        self.url = db.child("orders").child(order.name)
        self.order = order
        # self.start()


    def stream_handler(self, message):
        if message["path"] == "/status" and message["data"] == "Pending":
            print(message["event"]) # put
            print(message["path"]) # /-K7yGTTEp7O549EzTYtI
            print(message["data"]) # {'title': 'Pyrebase', "body": "etc..."}
        elif message["data"] == "Accepted":
            self.end()
        else:
            print('innnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn')
            print(message["data"]) 


    def start(self):
        self.order_stream = self.url.stream(self.stream_handler, stream_id="{}".format(self.order.name))

    def end(self):
        self.order_stream.close()

# def work(order):
#     a = Allocaton(order)



def order_watch(order):

    # enqueue(work, 'default', event='all', order=order)
    a = Allocaton(order)
    a.start()

    return True

