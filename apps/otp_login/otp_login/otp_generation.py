from __future__ import unicode_literals

import frappe
from frappe import _
import pyotp, os
from frappe.utils.background_jobs import enqueue
from pyqrcode import create as qrcreate
from six import BytesIO
from base64 import b64encode, b32encode
from frappe.utils import get_url, get_datetime, time_diff_in_seconds, cint
from otp_login.sms import sendSMS
from notifications.push import push_notification
from six import iteritems, string_types
from profiles.api import create_device

def authenticate_for_otp(user, pwd):
    '''Authenticate otp user before login.'''
    otp_secret = get_otpsecret_for_(user)
    token = int(pyotp.TOTP(otp_secret).now())
    tmp_id = frappe.generate_hash(length=8)
    cache_otp_data(user, pwd, token, otp_secret, tmp_id)
    verification_obj = get_verification_obj(user, token, otp_secret)
    # Save data in local
    frappe.local.response['verification'] = verification_obj
    frappe.local.response['tmp_id'] = tmp_id


def get_otpsecret_for_(user):
    '''Set OTP Secret for user even if not set.'''
    otp_secret = frappe.db.get_default(user + '_otpsecret')
    if not otp_secret:
        otp_secret = b32encode(os.urandom(10)).decode('utf-8')
        frappe.db.set_default(user + '_otpsecret', otp_secret)
        frappe.db.commit()
    return otp_secret


def cache_otp_data(user, pwd, token, otp_secret, tmp_id):
    '''Cache and set expiry for data.'''
    expiry_time = 300
    frappe.cache().set(tmp_id + '_token', token)
    frappe.cache().expire(tmp_id + '_token', expiry_time)
    for k, v in iteritems({'_usr': user, '_pwd': pwd, '_otp_secret': otp_secret}):
        frappe.cache().set("{0}{1}".format(tmp_id, k), v)
        frappe.cache().expire("{0}{1}".format(tmp_id, k), expiry_time)


def get_verification_obj(user, token, otp_secret):
    otp_issuer = frappe.db.get_value(
        'System Settings', 'System Settings', 'otp_issuer_name')
    verification_method = 'SMS'
    verification_obj = None
    if verification_method == 'SMS':
        verification_obj = process_2fa_for_sms(user, token, otp_secret)
    elif verification_method == 'Email':
        pass
    return verification_obj


def process_2fa_for_sms(user, token, otp_secret):
    '''Process sms method for 2fa.'''
    phone = frappe.db.get_value('User', user, ['phone', 'mobile_no'], as_dict=1)
    print(phone)
    phone = phone.mobile_no
    print(phone, '---------------------------------------------------------------++++++++')
    status = send_token_via_sms(otp_secret, user, token=token, phone_no=phone)
    verification_obj = {
        'token_delivery': status,
        'prompt': status and 'Enter verification code sent to {}'.format(phone[:4] + '******' + phone[-3:]),
        'method': 'SMS',
        'setup': status
    }
    return verification_obj


def send_token_via_sms(otpsecret, user, token=None, phone_no=None):
    '''Send token as sms to user. "temparatry" '''

    if not phone_no:
        return False

    hotp = pyotp.HOTP(otpsecret)
    otp = hotp.at(int(token))
    sms_args = {
        'message': 'Your verification code is {}'.format(otp)
    }
    sms_args['numbers'] = phone_no

    print(sms_args)
    notification = frappe.get_doc({
        'doctype': 'App Notifications',
        'title': 'OTP',
        'sender': user,
        'recipient': user,
        'message': sms_args['message'],
        'order_table': 'order_table',
        'order_name': '{}'.format(otp),
    })
    create_device(user)
    enqueue(push_notification, 'short', event='all', notification=notification)

    # sendSMS(**sms_args)

    # enqueue(method=sendSMS, queue='short', timeout=300, event=None,
    #     is_async=True, job_name=None, now=False, **sms_args)

    return True

