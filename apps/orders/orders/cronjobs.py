from __future__ import unicode_literals
import frappe
from frappe.utils import now
from fire_base_admin.fire_store import get_real_providers
from fire_base_admin.rdb import update_provider
from frappe.utils.background_jobs import enqueue


def match(order):
    vehecle = order['vh_re'].split("-")[1]
    providers = get_real_providers(order, vehecle,first=True)
    print("start.....")
    print(order)

    if len(providers) > 0:
        update_provider(order['name'], providers[0]['id'])
        frappe.db.set_value("Service Order", order['name'], "provider", providers[0]['id'])
    else:
        status = "Left-of"
        frappe.db.set_value("Service Order", order['name'], {"status":status, "provider": ""})


@frappe.whitelist()
def allocate_shedule():
    print('iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii  cronnnnnnnnnnnnnnnnnnnnnnn')
    least_time = frappe.db.get_single_value("Last Run", "on")
    current_time = now()

    filters = {
        'ride_date': ['>', least_time],
        'ride_date': ['<=', current_time],
        'status': 'Pending',
        'ride_type': 'scheduled',
        }
    
    orders = frappe.get_list('Service Order',
                fields=[
                    'name',
                    'vh_re',
                    'ride_date',
                    'start_lat',
                    'start_lon',],
                filters=filters,
                order_by="ride_date")

    for order in orders:
        enqueue(match, 'short', event='all', order=order)

    frappe.db.set_value("Last Run", "on", current_time)
        


