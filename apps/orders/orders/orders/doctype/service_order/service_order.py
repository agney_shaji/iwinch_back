# -*- coding: utf-8 -*-
# Copyright (c) 2019, Agney Shaji and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import math
# import frappe
from frappe.model.document import Document
from frappe.utils import flt, cint, cstr, today
import frappe
# from fire_base_admin.fire_store import create_order, delete_order
from fire_base_admin.fire_store import update_status
from fire_base_admin.rdb import create_order, delete_order, update_order


class ServiceOrder(Document):
	def validate(self):
		self.flags.is_new_doc = self.is_new()
		self.validate_offer()
		self.validate_services()
		self.validate_provider()

	def validate_offer(self):
		pass

	def validate_services(self):
		pass

	def validate_provider(self):
		pass

	def before_save(self):
		print('saveeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee')
		self = calulate_service_total(self)

	def after_insert(self):
		if self.is_new:
			cus_phno = frappe.db.get_value('Customer', self.customer, 'mobile_no')
			self.cus_phno = cus_phno
			create_order(self)
		elif self.provider and self.status == "Pending":
			update_status(self.provider, "engage")
			update_order(self)



	def on_trash(self):
		delete_order(self.name)


def calulate_service_total(order):
	if not order.vh_re:
		if frappe.form_dict.get('admin_area'):
			name = "{}-{}".format(frappe.form_dict.get('admin_area').upper(), frappe.form_dict.get('vh_type'))
			print(name)
			service = frappe.get_doc('Region Pricing', name)
			order.vh_re = service.name
			print(service.name)
			
			print('service.......................................................')
			qty = flt(order.total_qty)
			if qty <= service.base_km:
				qty = 0
			else:
				total = flt(service.base_price)
				qty -= service.base_km
			total = flt(service.base_price)
			extra = qty * service.km_price
			total += extra
			order.total = total
			order.commission_rate = service.com_per
			order.total_commission =  total*(flt(service.com_per)/flt(100))
			order.driver_pay = total - order.total_commission
			discount = 0.0
			if order.offer:
				offer = frappe.get_doc('Offers', order.offer)
				discount = total*(flt(offer.disc_per)/flt(100))
				if offer.max_disc_amt:
					discount = discount if discount < offer.max_disc_amt else offer.max_disc_amt
				total = total - discount
			order.discount_amount = discount
			order.net_total = total
			frac, whole = math.modf(total)
			rounding_adjustment = 0.0
			if frac != 0.0:
				if frac < 0.5:
					rounding_adjustment = 0 - frac
					total = whole
				else:
					rounding_adjustment = 0 + frac
					total = whole + 1
			order.rounding_adjustment = rounding_adjustment
			order.grand_total = total
		else:
			frappe.throw("servive no avilable")
	return order

