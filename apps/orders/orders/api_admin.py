from __future__ import unicode_literals
import frappe
from profiles.helper import buid_filters

@frappe.whitelist()
def list_orders(filters='{"name": ""}', start=0, order_by='creation desc', page_length=10):
    print(start)
    doctype = 'Service Order'
    filter_dict = buid_filters(filters)

    data = frappe.get_list(doctype, fields=[
        'name',
        'status',
        'DATE_FORMAT(creation, "%e-%m-%Y %H:%i") as creation',
        'payment_method',
        'name as id',
        'total',
        'DATE_FORMAT(ride_date, "%e-%m-%Y %H:%i") as ride_date',
        'ride_type'],
        filters=filter_dict,
        order_by=order_by,
        page_length=page_length,
        start=start)

    count = frappe.get_all(doctype, fields=["COUNT(*) as count"], filters=filter_dict)[0]['count']
    return {'orders': data, 'count': count}


@frappe.whitelist()
def list_invoices(filters='{"name": ""}', start=0, order_by='creation desc', page_length=10):

    # print(frappe.form_dict)

    doctype = 'Service Invoice'
    filter_dict = buid_filters(filters)
    # print(start)
    # print(order_by)
    print(filter_dict)
    data = frappe.get_all(doctype, fields=[
        'name',
        'status',
        'DATE_FORMAT(creation, "%e-%m-%Y %H:%i") as creation',
        'payment_method',
        'total',
        'name as id',
        'DATE_FORMAT(ride_date, "%e-%m-%Y %H:%i") as ride_date',
        'cancelled_by',
        'ride_type'],
        filters=filter_dict,
        order_by=order_by,
        page_length=page_length,
        start=start)

    count = frappe.get_all(doctype, fields=["COUNT(*) as count"], filters=filter_dict)[0]['count']
    print(count, 'countcountcountcount')
    return {'orders': data, 'count': count}

@frappe.whitelist()
def get_invoice(name):
    doctype = 'Service Invoice'
    # filter_dict = {"name": name}
    data = frappe.get_doc(doctype, name)
    count = 0
    # data = frappe.get_list(doctype, fields=[
    #     'name',
    #     'status',
    #     '`tabUser`.full_na'
    #     'DATE_FORMAT(creation, "%e-%m-%Y %H:%i") as creation',
    #     'payment_method',
    #     'name as id',
    #     'total',
    #     'DATE_FORMAT(ride_date, "%e-%m-%Y %H:%i") as ride_date',
    #     'ride_type'],
    #     filters=filter_dict,)

    # count = frappe.get_all(doctype, fields=["COUNT(*) as count"], filters=filter_dict)[0]['count']
    return {'orders': data, 'count': count}

@frappe.whitelist()
def get_order(name):
    doctype = 'Service Order'
    # filter_dict = {"name": name}
    data = frappe.get_doc(doctype, name)
    count = 0
    # data = frappe.get_list(doctype, fields=[
    #     'name',
    #     'status',
    #     '`tabUser`.full_na'
    #     'DATE_FORMAT(creation, "%e-%m-%Y %H:%i") as creation',
    #     'payment_method',
    #     'name as id',
    #     'total',
    #     'DATE_FORMAT(ride_date, "%e-%m-%Y %H:%i") as ride_date',
    #     'ride_type'],
    #     filters=filter_dict,)

    # count = frappe.get_all(doctype, fields=["COUNT(*) as count"], filters=filter_dict)[0]['count']
    return {'orders': data, 'count': count}

@frappe.whitelist()
def change_status(**kargs):
    order = frappe.get_doc('Service Order', kargs['name'])
    order.status = kargs['status']
    order.save()
    # try:
    #     vh_type = frappe.get_doc('Vehicle Type', provider.vh_type)
    #     provider.vh_name = vh_type.title
    # except:
    #     pass
    return {'orders': order, 'count': 0}