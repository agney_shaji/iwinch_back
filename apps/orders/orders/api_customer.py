from __future__ import unicode_literals
import frappe
import json
from profiles.provider_api import update_online_status
from orders.orders.doctype.service_order.service_order import calulate_service_total
from notifications.api import create_notifications

@frappe.whitelist()
def order_complete(order):
    # delete_order(order)
    return True


@frappe.whitelist()
def cancel_order(**kargs):

    order = frappe.get_doc('Service Order', kargs['order_id'])
    order_table = 'Service Order'
    

    invoice = frappe.get_doc({
        'doctype': 'Service Invoice',
        'customer': order.customer,
        'provider': order.provider,
        'total_qty': order.total_qty,
        'total': order.total,
        'net_total': order.net_total,
        'total_taxes_and_charges': order.total_taxes_and_charges,
        'discount_amount': order.discount_amount,
        'grand_total': order.grand_total,
        'rounding_adjustment': order.rounding_adjustment,
        'rounded_total': order.rounded_total,
        'driver_pay': order.driver_pay,
        'offer': order.offer,
        'payment_method': order.payment_method,
        'amended_from': order.name,
        'status': 'Cancelled',
        'payment_status': order.payment_status,
        'start_lat': order.start_lat,
        'start_lon': order.start_lon,
        'end_lat': order.end_lat,
        'end_lon': order.end_lon,
        'ride_date': order.ride_date,
        'ride_type': order.ride_type,
        'cancelled_by': 'customer',
        'vh_re': order.vh_re,
    })
    invoice.flags.ignore_permissions = True
    invoice.insert()
    order.delete()
    if invoice.provider:
        update_online_status(is_customer=True, provider=invoice.provider, is_online=1)
    return True



@frappe.whitelist()
def estimate_order(**kargs):
    doctype = 'Service Order'

    offer = kargs.get('offer', '')

    order = frappe.get_doc({
        'doctype': doctype,
        'customer': frappe.session.user,
        'total_qty':  kargs['total_qty'],
        'offer':  offer if offer != 'null' else '',
        'status': 'Pending',
    })
    order = calulate_service_total(order)
    return order

@frappe.whitelist()
def list_customer_active_orders(limit_start=0):
    doctype = 'Service Order'
    filters = [['customer', '=', '{}'.format(frappe.session.user)]]
    data = frappe.get_list(doctype, fields=[
        'name',
        'customer',
        'provider',
        'creation',
        'start_lat',
        'start_lon',
        'end_lat',
        'end_lon',
        'ride_type',
        'grand_total',
        'status'],
        filters=filters,
        order_by='creation desc',
        limit_start=limit_start)
    return data


@frappe.whitelist()
def list_customer_previous_orders(limit_start=0):
    doctype = 'Service Invoice'    
    
    filters = [['customer', '=', '{}'.format(frappe.session.user)]]


    data = frappe.get_list(doctype, fields=[
        'name',
        'customer',
        'provider',
        'creation',
        'start_lat',
        'start_lon',
        'end_lat',
        'end_lon',
        'ride_type',
        'grand_total',
        'status'],
        filters=filters,
        order_by='creation desc',
        limit_page_length=20,
        limit_start=limit_start)
    return data


@frappe.whitelist()
def send_notification(order, provider):
    
    notification = frappe.get_doc({
            'doctype': 'App Notifications',
            'title': "Pending",
            'sender': frappe.session.user,
            'recipient': provider,
            'message': "New order waiting for conformation",
            'order_table': "Sales Order",
            'order_name': order,
        })
    create_notifications(notification)