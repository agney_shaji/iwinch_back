from __future__ import unicode_literals
import frappe
import json
from frappe.utils import now, today
from discounts.api import offer_detail
from profiles.api import get_logged_customer, get_logged_provider, get_provider_detail
from fire_base_admin.rdb import order_watch


@frappe.whitelist()
def create_order(**kargs):
    doctype = 'Service Order'
    now_date = now()

    pay_method = kargs.get('payment_method', 'Cash')
    offer = kargs.get('offer', '')
    ride_date = kargs.get('ride_date', now_date)
    ride_date = ride_date if ride_date != '' else now_date
    ride_type = 'immediate' if ride_date == now_date else 'scheduled'

    order = frappe.get_doc({
        'doctype': doctype,
        'customer': frappe.session.user,
        'total_qty':  kargs['total_qty'],
        'offer':  offer if offer != 'null' else '',
        'payment_method': pay_method,
        'status': 'Pending',
        'start_lat': kargs['start_lat'],
        'start_lon': kargs['start_lon'],
        'end_lat': kargs['end_lat'],
        'end_lon': kargs['end_lon'],
        'ride_date': ride_date,
        'ride_type': ride_type,
    })

    order.flags.ignore_permissions = True
    order.insert()
    return order

@frappe.whitelist()
def create_order_provider(**kargs):
    frappe.db.set_value(
            'Service Order', kargs['order'], 'provider', kargs['provider'])


@frappe.whitelist()
def validate_offer(**kargs):
    offer_name = frappe.db.get_value("Offers", filters={"coupon_code": kargs['coupon_code']}, fieldname="name")
    print(offer_name)
    offer = offer_detail(offer_name)
    if offer:
        return offer
    return None

@frappe.whitelist()
def order_detail(order_name):
    try:
        order = frappe.get_doc('Service Invoice', order_name)
    except:
        order = frappe.get_doc('Service Order', order_name)
    return order

@frappe.whitelist()
def allocateorder(order_name):
    order = frappe.get_doc('Service Order', order_name)
    order_watch(order)
