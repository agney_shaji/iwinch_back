from __future__ import unicode_literals
import frappe
from frappe.utils import flt
from profiles.api import get_customer_detail
from otp_login.api import create_token
from fire_base_admin.auth_fire import validate_user
import os
from base64 import b32encode


@frappe.whitelist(allow_guest=True)
def register_rider(**kargs):

    uid = validate_user(str(kargs['id_token']))
    user = frappe.db.get_value("User", filters={"uuid": uid}, fieldname="name")

    if not user:
        temp_id1 =  b32encode(os.urandom(10)).decode('utf-8')
        temp_id2 =  b32encode(os.urandom(5)).decode('utf-8')
        user_details = frappe.get_doc({
            'doctype': 'User',
            'first_name': temp_id2,
            'email': '{}@{}.uae'.format(temp_id1, temp_id2),
            'uuid': uid,
            'is_rider': 1,
        })
        user_details.flags.no_welcome_mail = True
        user_details.flags.ignore_permissions = True
        user_details.add_roles('Purchase User')
        user_details.save()
        user = user_details.name
        doctype = 'Customer'
        profile = frappe.get_doc({
            'doctype': doctype,
            'account_manager': user,
            'first_name': kargs.get('first_name', ''),
            'mobile_no': kargs.get('mobile_no', ''),
            'email_id': kargs.get('email_id', ''),
            'second_name': kargs.get('surname', ''),
            'full_name': kargs.get('first_name', '') + kargs.get('surname', ''),
            'image': kargs.get('image', ''),
            })
        profile.flags.ignore_permissions = True
        profile.save()
        status = True
    else:
        status = False
    token = create_token(user)
    token['status'] = status 
    return token


@frappe.whitelist()
def create_profile(**kargs):
    doctype = 'Customer'
    profile = frappe.get_doc({
        'doctype': doctype,
        'account_manager': frappe.session.user,
        'first_name': kargs.get('first_name', ''),
        'mobile_no': kargs.get('mobile_no', ''),
        'email_id': kargs.get('email_id', ''),
        'second_name': kargs.get('surname', ''),
        'full_name': kargs.get('first_name', '') + kargs.get('surname', ''),
        'image': kargs.get('image', ''),
    })
    profile.flags.ignore_permissions = True
    profile.insert()
    return profile


@frappe.whitelist()
def create_feedback(**kargs):
    doctype = 'Customer Feedback'

    provider_name, customer_name = frappe.db.get_value('Service Invoice',
                                             kargs['order'], ['provider', 'customer'])

    feedback = frappe.get_doc({
        'doctype': doctype,
        'rating': kargs['rating'],
        'order': kargs['order'],
        'customer': customer_name,
        'provider': provider_name,
        'description': kargs.get('description', None),
    })
    feedback.flags.ignore_permissions = True
    feedback.save()



    customer = get_customer_detail(customer_name)
    customer.total_feefback += 1
    customer.total_rating += flt(feedback.rating)
    customer.rating = customer.total_rating / customer.total_feefback

    frappe.db.set_value('Customer', customer_name, {
    'total_feefback': customer.total_feefback,
    'total_rating':  customer.total_rating,
    'rating': customer.rating,
    })
    return feedback
