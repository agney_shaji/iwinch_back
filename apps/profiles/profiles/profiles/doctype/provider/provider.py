# -*- coding: utf-8 -*-
# Copyright (c) 2019, Agney and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
# import frappe
from frappe.model.document import Document
import frappe
from frappe import _
import re

class Provider(Document):

	# pass
	def validate(self):
		self.flags.is_new_doc = self.is_new()

	def before_save(self):
		if self.flags.is_new_doc:
			frappe.db.set_value('User', self.account_manager, 'mobile_no', self.mobile_no)
			self.new_reg = 1
		if self.disabled == 0 and self.new_reg == 1:
			self.new_reg = 0
		if self.disabled == 1:
			frappe.db.set_value('User', self.account_manager, 'enabled', 0)
		else:
			frappe.db.set_value('User', self.account_manager, 'enabled', 1)


	def on_trash(self):
		frappe.db.delete('User', {'name': self.name})
