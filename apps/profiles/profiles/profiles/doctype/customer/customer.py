# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# License: GNU General Public License v3. See license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Customer(Document):

	def validate(self):
		self.flags.is_new_doc = self.is_new()
	
	def before_save(self):
		if self.flags.is_new_doc:
			frappe.db.set_value('User', self.account_manager, 'mobile_no', self.mobile_no)
		if self.disabled == 1:
			frappe.db.set_value('User', self.account_manager, 'enabled', 0)
		else:
			frappe.db.set_value('User', self.account_manager, 'enabled', 1)


	def on_trash(self):
		frappe.db.delete('User', {'name': self.name})
