from __future__ import unicode_literals
import frappe
# from profiles.api import get_logged_customer, get_logged_provider
# from frappe.utils.nestedset import NestedSet, get_ancestors_of, get_descendants_of



@frappe.whitelist()
def get_user_role():
    
    return frappe.get_roles(frappe.session.user)[0]

@frappe.whitelist()
def get_logged_customer():
    doctype = 'Customer'
    customer = frappe.get_doc(doctype,frappe.session.user)
    return customer

@frappe.whitelist()
def get_logged_provider():
    doctype = 'Provider'
    provider = frappe.get_doc(doctype,frappe.session.user)
    return provider

@frappe.whitelist()
def get_provider_detail(pro_name):
    doctype = 'Provider'
    provider = frappe.get_doc(doctype, pro_name)
    return provider

@frappe.whitelist()
def get_customer_detail(cus_name):
    doctype = 'Customer'
    customer = frappe.get_doc(doctype, cus_name)
    return customer

# @frappe.whitelist()
# def get_providers(service='', radius=10):
#     res = get_real_providers()
#     return res


@frappe.whitelist()
def update_customer_name(**kargs):
    user_details = frappe.get_doc("User", frappe.session.user)
    user_details.first_name = kargs['first_name']
    user_details.last_name = kargs['surname']
    user_details.flags.ignore_permissions = True
    user_details.save()
    customer = get_logged_customer()
    customer.first_name = kargs['first_name']
    customer.second_name = kargs['surname']
    customer.full_name = customer.first_name + " " +customer.second_name
    customer.email_id = kargs['email_id']
    customer.image = kargs['image']
    customer.flags.ignore_permissions = True
    customer.save()
    return customer

@frappe.whitelist()
def update_provider_name(**kargs):
    user_details = frappe.get_doc("User", frappe.session.user)
    user_details.first_name = kargs['first_name']
    user_details.last_name = kargs['surname']
    user_details.flags.ignore_permissions = True
    user_details.save()
    privider = get_logged_provider()
    privider.first_name = kargs['first_name']
    privider.second_name = kargs['surname']
    privider.full_name = privider.first_name + " " +privider.second_name
    privider.email_id = kargs['email_id']
    privider.image = kargs['image']
    privider.flags.ignore_permissions = True
    privider.save()
    
    return privider


# @frappe.whitelist(allow_guest=True)
def create_device(user):
    doctype = 'Devices'
    if frappe.db.exists({'doctype': doctype, 'user': user}):
        name = frappe.db.get_value(doctype, {'user': user}, ['name'])
        device = frappe.get_doc(doctype, name)
        device.token = frappe.form_dict.get('token')
        device.imei = frappe.form_dict.get('imei')
        device.flags.ignore_permissions = True
        device.save()
    else:
        device = frappe.get_doc({
            'doctype': doctype,
            'user': user,
            'token':frappe.form_dict.get('token'),
            'imei':frappe.form_dict.get('imei')})
        device.flags.ignore_permissions = True
        device.insert()
    return device
